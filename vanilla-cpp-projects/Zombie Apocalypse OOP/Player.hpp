#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include <string>
using namespace std;

struct Player
{
    Player();

    int food;
    string name;
    int maxHealth;
    int health;
    string location;
};

#endif
