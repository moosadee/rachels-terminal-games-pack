#include <cstdlib>
#include <ctime>
using namespace std;

#include "Game.hpp"

int main()
{
    srand( time( NULL ) );
    Game game;
    game.Run();

    return 0;
}
