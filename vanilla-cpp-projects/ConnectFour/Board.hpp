#ifndef _BOARD_HPP
#define _BOARD_HPP

class Board
{
public:
    Board();
    void Clear();
    void Draw();
    bool AddPiece( int x, char symbol );
    char CheckForWinner();
    int GetWidth();
    int GetHeight();

private:
    char m_board[7][7];
    const int WIDTH = 7;
    const int HEIGHT = 7;
    const char EMPTY = '.';
};

#endif
