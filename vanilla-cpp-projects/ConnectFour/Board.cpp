#include "Board.hpp"

#include "utilities/ScreenDrawer.hpp"

Board::Board()
{
    Clear();
}

void Board::Clear()
{
    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            m_board[x][y] = EMPTY;
        }
    }
}

void Board::Draw()
{
    int offsetX = 1;
    int offsetY = 1;

    for ( int x = 0; x < WIDTH; x++ )
    {
        int adjustedX = ( x * 2 ) + offsetX;
        int adjustedY = offsetY - 1;
        ScreenDrawer::Set( adjustedX, adjustedY, x );
    }

    for ( int y = 0; y < HEIGHT; y++ )
    {
        for ( int x = 0; x < WIDTH; x++ )
        {
            int adjustedX = ( x * 2 ) + offsetX;
            int adjustedY = y + offsetY;
            ScreenDrawer::Set( adjustedX, adjustedY, m_board[x][y] );
        }
    }
}

bool Board::AddPiece( int x, char symbol )
{
    int availableY = -1;
    // Check to see next available space
    for ( int y = HEIGHT-1; y >= 0; y-- )
    {
        if ( m_board[x][y] == EMPTY )
        {
            availableY = y;
            break;
        }
    }

    if ( availableY == -1 )
    {
        return false; // No space in this column
    }

    m_board[x][availableY] = symbol;
    return true;
}

char Board::CheckForWinner()
{
    // Gotta check for four in a row
    return EMPTY;
}

int Board::GetWidth()
{
    return WIDTH;
}

int Board::GetHeight()
{
    return HEIGHT;
}
