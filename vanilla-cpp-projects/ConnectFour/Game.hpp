#ifndef _GAME_HPP
#define _GAME_HPP

#include "Board.hpp"
#include "Player.hpp"

class Game
{
public:
    void Run();

private:
    void Play();
    void Init();

    Player m_players[2];
    Board m_board;
};

#endif
