#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <chrono>
#include <thread>
using namespace std;

void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

void Sleep( int milliseconds )
{
    std::this_thread::sleep_for( std::chrono::milliseconds( milliseconds ) );
}

struct Frame
{
    vector<string> lines;

    void Display()
	{
	    for ( size_t i = 0; i < lines.size(); i++ )
	    {
		cout << lines[i] << endl;
	    }
	}
};

vector<Frame> LoadAnimation( string filepath, int screenHeight )
{
    vector<Frame> frames;
    
    ifstream input( filepath );
    if ( input.fail() )
    {
	cout << "ERROR: \"" << filepath << "\" not found!" << endl;
	return frames;
    }

    int lineCounter = 0;
    string buffer;
    Frame newFrame;
    while ( getline( input, buffer ) )
    {
	newFrame.lines.push_back( buffer );
	lineCounter++;
	if ( lineCounter == screenHeight )
	{
	    lineCounter = 0;
	    frames.push_back( newFrame );
	    newFrame.lines.clear();
	}
    }
    
    return frames;
}

int main( int argCount, char* args[] )
{
    const int WIDTH=80;
    const int HEIGHT=20;

    if ( argCount == 1 )
    {
	cout << "EXPECTED: " << args[0] << " filepath fps" << endl;
	return 1;
    }

    string animationPath = string( args[1] );
    int fps = stoi( args[2] );
    cout << "Loading \"" << animationPath << "\"..." << endl;
    vector<Frame> frames = LoadAnimation( animationPath, HEIGHT );
    cout << frames.size() << " frame(s) loaded." << endl;

    size_t f = 0;
    while ( true )
    {
	ClearScreen();
	frames[f].Display();
	f++;
	if ( f == frames.size() )
	{
	    f = 0;
	}
	Sleep( 1000 / fps );
    }
    
    
    return 0;
}
