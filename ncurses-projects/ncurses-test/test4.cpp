#include <iostream>
using namespace std;

#include "../engine/TerminalApp.h"
#include <ncurses.h>

int main()
{
  TerminalApp::Setup();

  int player_x = 40;
  int player_y = 10;

  bool done = false;
  while ( !done )
    {
      // Draw map
      for ( int y = 1; y < TerminalApp::screenHeight; y++ )
        {
	  for ( int x = 0; x < TerminalApp::screenWidth; x++ )
            {
	      if ( y == 1 || y == TerminalApp::screenHeight-1 || x == 0 || x == TerminalApp::screenWidth-1 )
                {		  
		  TerminalApp::DrawChar( x, y, "YELLOW-ON-RED", '#' );
                }
	      else
                {
		  TerminalApp::DrawChar( x, y, "YELLOW-ON-GREEN", 'w' );
                }
            }
        }

      // Example HUD
      for ( int x = 0; x < TerminalApp::screenWidth; x++ )
	{
	  TerminalApp::DrawChar( x, 0, "WHITE-ON-BLACK", ' ' );
	}
      TerminalApp::DrawText( 0, 0, "WHITE-ON-BLACK", "(" + TerminalApp::ToString( player_x ) + ", " + TerminalApp::ToString( player_y ) + ")" );

      // Draw player
      TerminalApp::DrawChar( player_x, player_y, "BLACK-ON-GREEN", '@' );
      TerminalApp::RefreshScreen();

      // Updates and inputs
      TerminalApp::Update();

      if ( TerminalApp::IsKeyPressed( KEY_LEFT ) )
	{
	  player_x--;
	}
      else if ( TerminalApp::IsKeyPressed( KEY_RIGHT ) )
	{
	  player_x++;
	}
      else if ( TerminalApp::IsKeyPressed( KEY_UP ) )
        {
	  player_y--;
        }
      else if ( TerminalApp::IsKeyPressed( KEY_DOWN ) )
        {
	  player_y++;
        }
    }

  TerminalApp::Teardown();

  return 0;
}
