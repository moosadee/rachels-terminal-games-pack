
#include <ncurses.h>
#include <iostream>
using namespace std;

int main()
{
  initscr();
  //	raw();
  keypad( stdscr, TRUE );
  noecho();
  start_color();

  init_pair( 1, COLOR_BLACK, COLOR_BLUE );	// Walls
  init_pair( 2, COLOR_WHITE, COLOR_CYAN );	// Floors
  init_pair( 3, COLOR_RED, COLOR_MAGENTA );
  init_pair( 4, COLOR_BLACK, COLOR_WHITE );

  int player_x = 40;
  int player_y = 10;

  bool done = false;
  while ( !done )
    {

      // Draw map
      for ( int y = 0; y < 25; y++ )
        {
	  for ( int x = 0; x < 80; x++ )
            {
	      if ( y == 0 || y == 24 || x == 0 || x == 79 )
                {
		  attron( COLOR_PAIR( 1 ) );
		  mvaddch( y, x, 'W' );
		  attroff( COLOR_PAIR( 1 ) );
                }
	      else
                {
		  attron( COLOR_PAIR( 2 ) );
		  mvaddch( y, x, 'F' );
		  attroff( COLOR_PAIR( 2 ) );
                }
            }
        }

      // Draw player
      attron( COLOR_PAIR( 3 ) );
      mvaddch( player_y, player_x, 'X' );
      attroff( COLOR_PAIR( 3 ) );


      refresh();

      int input = getch();
      // F4 to quit?
      if ( input == KEY_F( 4 ) )
        {
	  done = true;
        }
      else if ( input == KEY_LEFT )
        {
	  player_x--;
        }
      else if ( input == KEY_RIGHT )
        {
	  player_x++;
        }
      else if ( input == KEY_UP )
        {
	  player_y--;
        }
      else if ( input == KEY_DOWN )
        {
	  player_y++;
        }


      attron( COLOR_PAIR( 4 ) );
      mvprintw( 0, 0, "You entered %d", input );
    }
  endwin();

  return 0;
}
