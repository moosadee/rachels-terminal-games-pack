#include <iostream>
using namespace std;

#include "../engine/TerminalApp.h"
#include <ncurses.h>

int main()
{
  TerminalApp::Setup();
  TerminalApp::SetDialogBorderColor( "WHITE-ON-WHITE" );
  TerminalApp::SetDialogBackgroundColor( "BLUE-ON-BLUE" );
  TerminalApp::SetDialogTextColor( "WHITE-ON-BLUE" );

  int player_x = 50;
  int player_y = 10;

  bool done = false;
  while ( !done )
    {
        std::vector<std::string> colorNames = { "BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE" };

	int x = 0;
	int y = 0;
	int ch = 65;
	for ( auto& col : colorNames )
	  {
	    std::string color = "WHITE-ON-" + col;
	    TerminalApp::DrawChar( x, y, color, char( ch ) );
	    ch++;
	    x++;
	    if ( x >= TerminalApp::screenWidth )
	      {
		x = 0;
		y++;
	      }
	  }


      TerminalApp::RefreshScreen();

      // Dialog box
      TerminalApp::DrawBorderedText( 20, 5, "Hello, world!" );
      TerminalApp::DrawBorderedText( 20, 15, { "Line1", "Line 2", "Line 3?!" }, "YELLOW-ON-YELLOW", "RED-ON-RED", "WHITE-ON-RED" );
      
      TerminalApp::RefreshScreen();
      
      // Updates and inputs
      TerminalApp::Update();

      if ( TerminalApp::IsKeyPressed( KEY_LEFT ) )
	{
	  player_x--;
	}
      else if ( TerminalApp::IsKeyPressed( KEY_RIGHT ) )
	{
	  player_x++;
	}
      else if ( TerminalApp::IsKeyPressed( KEY_UP ) )
        {
	  player_y--;
        }
      else if ( TerminalApp::IsKeyPressed( KEY_DOWN ) )
        {
	  player_y++;
        }
    }

  TerminalApp::Teardown();

  return 0;
}
