echo "Build test1"
g++ test1.cpp -o test1.out -lncurses

echo "Build test2"
g++ test2.cpp -o test2.out -lncurses

echo "Build test3"
g++ test3.cpp -o test3.out -lncurses

echo "Build test4"
g++ test4.cpp ../engine/*.h ../engine/*.cpp -o test4.out -lncurses -std=c++17

echo "Build test5"
g++ test5.cpp ../engine/*.h ../engine/*.cpp -o test5.out -lncurses -std=c++17

echo "Build test6"
g++ test6.cpp ../engine/*.h ../engine/*.cpp -o test6.out -lncurses -std=c++17

echo "Build test7"
g++ test7.cpp ../engine/*.h ../engine/*.cpp -o test7.out -lncurses -std=c++17

echo "Build test8"
g++ test8.cpp ../engine/*.h ../engine/*.cpp -o test8.out -lncurses -std=c++17
