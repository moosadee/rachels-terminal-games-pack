#include <iostream>
using namespace std;

#include "../engine/TerminalApp.h"
#include "../engine/PpmImage.h"
#include <ncurses.h>

int main()
{
  TerminalApp::Setup();

  PpmImage image( "scene.ppm" );
  
  bool done = false;
  while ( !done )
    {
      for ( auto& pixel : image.GetPixels() )
	{
	  TerminalApp::DrawChar( pixel.x, pixel.y, "WHITE-ON-" + pixel.ncursesColor, ' ' );
	}

      
      TerminalApp::RefreshScreen();
      
      // Updates and inputs
      TerminalApp::Update();
    }

  TerminalApp::Teardown();

  return 0;
}
