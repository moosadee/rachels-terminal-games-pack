#ifndef _STATE_BASE
#define _STATE_BASE
#include "../engine/TerminalApp.h"
#include "../engine/PpmImage.h"
#include <string>

class State_Base
{
 public:
  State_Base();
  virtual ~State_Base() { }
  virtual void Setup() = 0;
  virtual void Teardown() = 0;
  virtual void Update() = 0;
  virtual void Draw() = 0;
  virtual std::string GetNextState() { return m_nextState; }

 protected:
  int m_screenWidth, m_screenHeight;
  std::string m_nextState;
  PpmImage m_background;
};

#endif
