#ifndef _STATE_GAME
#define _STATE_GAME
#include "State_Base.h"

class State_Game : public State_Base
{
 public:
  State_Game();
  virtual ~State_Game() { }
  virtual void Setup();
  virtual void Teardown();
  virtual void Update();
  virtual void Draw();

private:
  std::string m_subState;
};

#endif
