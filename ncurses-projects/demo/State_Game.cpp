#include "State_Game.h"
#include "../engine/TerminalApp.h"

State_Game::State_Game()
{
  TerminalApp::Log( "Function begin", "State_Game::State_Game" );
  m_nextState = "";
}

void State_Game::Setup()
{
  TerminalApp::Log( "Function begin", "State_Game::Setup" );
  State_Base::Setup();
  TerminalApp::SetDialogBorderColor( "BLACK-ON-WHITE" );
  TerminalApp::SetDialogBackgroundColor( "WHITE-ON-BLACK" );
  TerminalApp::SetDialogTextColor( "WHITE-ON-BLACK" );
  m_subState = "PLAY";

  // Why is adding to the vector causing segfaults?
  //m_options.push_back( "    [1] Play    " );
  //m_options.push_back( "    [2] Help    " );
  //m_options.push_back( "    [0] Quit    " );
}

void State_Game::Teardown()
{
  TerminalApp::Log( "Function begin", "State_Game::Teardown" );
}

void State_Game::Update()
{
  TerminalApp::Update();

  if ( TerminalApp::input == 48 ) // 0 - quit
    {
    }
  else if ( TerminalApp::input == 49 ) // 1 - play
    {
    }
  else if ( TerminalApp::input == 50 ) // 2 - help
    {
    }
}

void State_Game::Draw()
{
  m_background.Draw();

  //TerminalApp::DrawRectangle( 0, 0, m_screenWidth, m_screenHeight, "CYAN-ON-BLUE", '/' );

  if ( m_subState == "PLAY" )
    {
    }

  // Debug: Key code
  TerminalApp::DrawLine( 0, TerminalApp::screenHeight, TerminalApp::screenWidth, TerminalApp::screenHeight, "WHITE-ON-BLUE", ' ' );
  TerminalApp::DrawText( 0, TerminalApp::screenHeight, "WHITE-ON-BLUE", TerminalApp::ToString( TerminalApp::input ) );

  TerminalApp::RefreshScreen();
}
