#include "State_Title.h"
#include "../engine/TerminalApp.h"

State_Title::State_Title()
{
  TerminalApp::Log( "Function begin", "State_Title::State_Title" );
  m_subState = "TITLE";
}

void State_Title::Setup()
{
  TerminalApp::Log( "Function begin", "State_Title::Setup" );
  State_Base::Setup();
  TerminalApp::SetDialogBorderColor( "BLACK-ON-WHITE" );
  TerminalApp::SetDialogBackgroundColor( "WHITE-ON-BLACK" );
  TerminalApp::SetDialogTextColor( "WHITE-ON-BLACK" );
  m_title = "    EXAMPLE PROGRAM    ";
  m_background.Open( "background.ppm" );

  // Why is adding to the vector causing segfaults?
  //m_options.push_back( "    [1] Play    " );
  //m_options.push_back( "    [2] Help    " );
  //m_options.push_back( "    [0] Quit    " );
}

void State_Title::Teardown()
{
  TerminalApp::Log( "Function begin", "State_Title::Teardown" );
}

void State_Title::Update()
{
  TerminalApp::Update();

  if ( TerminalApp::input == 48 ) // 0 - quit
    {
      if ( m_subState == "TITLE" )
        {
          m_nextState = "QUIT";
        }
      else
        {
          m_subState = "TITLE";
        }
    }
  else if ( TerminalApp::input == 49 ) // 1 - play
    {
      m_nextState = "GAME";
    }
  else if ( TerminalApp::input == 50 ) // 2 - help
    {
      m_subState = "HELP";
    }
}

void State_Title::Draw()
{
  m_background.Draw();

  //TerminalApp::DrawRectangle( 0, 0, m_screenWidth, m_screenHeight, "CYAN-ON-BLUE", '/' );

  if ( m_subState == "TITLE" )
    {
      TerminalApp::DrawBorderedTexts( m_screenWidth/2 - m_title.size()/2 - 2, 5, { m_title } );

      TerminalApp::DrawBorderedTexts( 33, m_screenHeight/2, {
	  " [1] PLAY ",
	  " [2] HELP ",
	  " [0] QUIT "
	} );
    }
  else if ( m_subState == "HELP" )
    {
      TerminalApp::DrawBorderedTexts( 4, 5, {
	  "Lorem ipsum dolor sit amet, consectetur adipiscing elit,",
	  "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
	  "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
	  "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in",
	  "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla",
	  "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in ",
	  "culpa qui officia deserunt mollit anim id est laborum.",
	  "",
	  "[0] BACK"
	} );
    }

  // Debug: Key code
  TerminalApp::DrawLine( 0, TerminalApp::screenHeight, TerminalApp::screenWidth, TerminalApp::screenHeight, "WHITE-ON-BLUE", ' ' );
  TerminalApp::DrawText( 0, TerminalApp::screenHeight, "WHITE-ON-BLUE", TerminalApp::ToString( TerminalApp::input ) );

  TerminalApp::RefreshScreen();
}
