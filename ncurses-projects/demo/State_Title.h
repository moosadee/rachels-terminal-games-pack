#ifndef _STATE_TITLE
#define _STATE_TITLE

#include <string>
#include <vector>
#include "State_Base.h"

class State_Title : public State_Base
{
 public:
  State_Title();
  virtual ~State_Title() { }
  virtual void Setup();
  virtual void Teardown();
  virtual void Update();
  virtual void Draw();

private:
  std::string m_title;
  std::string m_subState;
};

#endif
