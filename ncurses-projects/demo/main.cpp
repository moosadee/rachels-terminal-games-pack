#include <iostream>
using namespace std;

#include "../engine/TerminalApp.h"
#include "../engine/PpmImage.h"
#include "State_Title.h"

int main()
{
  TerminalApp::Setup();

  TerminalApp::Log( "Create state" );
  State_Base* state = new State_Title;
  state->Setup();

  TerminalApp::Log( "Start program loop" );
  bool done = false;
  while ( !done )
    {
      state->Draw();
      state->Update();

      if ( state->GetNextState() == "QUIT" )
	{
	  done = true;
	}
    }

  TerminalApp::Log( "Teardown" );
  state->Teardown();
  delete state;
  
  TerminalApp::Teardown();
  return 0;
}
