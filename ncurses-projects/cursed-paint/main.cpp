#include <iostream>
#include <string>
using namespace std;

#include "../engine/TerminalApp.h"
#include "../engine/PpmImage.h"
#include <ncurses.h>

int main( int argCount, char* args[] )
{
  int imageWidth = -1;
  int imageHeight = -1;
  std::string filename = "wip.ppm";
  if ( argCount == 2 )
    {
      filename = std::string( args[1] );
    }
  else if ( argCount == 3 )
    {
      imageWidth = stoi( args[1] );
      imageHeight = stoi( args[2] );
    }
   else
    {
      std::cout << "Please provide width and height or filename. Examples:" << std::endl;
      std::cout << "[1] New image:  " << args[0] << " width height" << std::endl;
      std::cout << "[2] Load image: " << args[0] << " filename" << std::endl;
      return 1;
    }

  TerminalApp::Setup();

  int cursorX = imageWidth/2;
  int cursorY = imageHeight/2;
  std::string cursorColor = "BLACK";
  int brushSize = 1;
  bool pencilDown = false;
  std::string status = "";

  PpmImage image;
  if ( imageWidth == -1 )
    {
      // Load image
      image.Open( filename );
      imageWidth = image.GetWidth();
      imageHeight = image.GetHeight();
    }
  else
    {
      // New image
      image.Create( filename, imageWidth, imageHeight );
    }
  
  bool done = false;
  while ( !done )
    {
      // Draw screen
      for ( auto& pixel : image.GetPixels() )
	{
	  char symbol = ' ';
	  std::string color = "WHITE-ON-" + pixel.ncursesColor;
	  if ( cursorX == pixel.x && cursorY == pixel.y )
	    {
	      symbol = ( pencilDown ) ? 'x' : '.';
	      color = PpmImage::GetGoodTextColor( cursorColor ) + "-ON-" + cursorColor;
	    }
	  
	  TerminalApp::DrawChar( pixel.x, pixel.y, color, symbol );
	}

      // HUD INFO
      TerminalApp::DrawText( 0, imageHeight, "WHITE-ON-BLACK",    "[1] BLACK" );
      TerminalApp::DrawText( 10, imageHeight, "WHITE-ON-RED",     "[2] RED  " );
      TerminalApp::DrawText( 20, imageHeight, "BLACK-ON-GREEN",   "[3] GREEN" );
      TerminalApp::DrawText( 30, imageHeight, "BLACK-ON-YELLOW",  "[4] YELLO" );
      TerminalApp::DrawText( 40, imageHeight, "WHITE-ON-BLUE",    "[5] BLUE " );
      TerminalApp::DrawText( 50, imageHeight, "BLACK-ON-MAGENTA", "[6] MAGEN" );
      TerminalApp::DrawText( 60, imageHeight, "BLACK-ON-CYAN",    "[7] CYAN " );
      TerminalApp::DrawText( 70, imageHeight, "BLACK-ON-WHITE",   "[8] WHITE" );
      TerminalApp::DrawText( 0, imageHeight+1, "WHITE-ON-BLACK", "[WASD] MOVE CURSOR  [H] PENCIL POKE [J] PENCIL DOWN  [K] PENCIL UP  [U] FILL COLOR (ALL)" );
      TerminalApp::DrawText( 0, imageHeight+2, "WHITE-ON-BLACK", "[F5] SAVE" );
      TerminalApp::DrawLine( 0, imageHeight+3, imageWidth, imageHeight+3, "BLACK-ON-BLUE", ' ' );
      TerminalApp::DrawText( 0, imageHeight+4, "WHITE-ON-BLACK",
			     "Key: " + TerminalApp::ToString( TerminalApp::input ) );
      std::string pencilState = ( pencilDown ) ? "Pencil is down" : "Pencil is up";
      TerminalApp::DrawText( 30, imageHeight+4, "WHITE-ON-BLACK", pencilState );
      TerminalApp::DrawText( 0, imageHeight+3, "WHITE-ON-BLACK", status );
      
      TerminalApp::RefreshScreen();
      
      bool moved = false;
      // Updates and inputs
      TerminalApp::Update();
      if ( TerminalApp::IsKeyPressed( KEY_LEFT ) ) { cursorX--; moved = true; }
      else if ( TerminalApp::IsKeyPressed( KEY_RIGHT ) ) { cursorX++; moved = true; }
      if ( TerminalApp::IsKeyPressed( KEY_UP ) ) { cursorY--; moved = true; }
      else if ( TerminalApp::IsKeyPressed( KEY_DOWN ) ) { cursorY++; moved = true; }

      if ( TerminalApp::IsKeyPressed( KEY_F(5) ) )
	{
	  image.Save();
	  status = "Saved to " + image.GetFilename();
	}
      
      switch( TerminalApp::input )
	{
	  // Color select
	case 49: cursorColor = "BLACK";    status = "BLACK brush"; break;
	case 50: cursorColor = "RED";      status = "RED brush"; break;
	case 51: cursorColor = "GREEN";    status = "GREEN brush"; break;
	case 52: cursorColor = "YELLOW";   status = "YELLOW brush"; break;
	case 53: cursorColor = "BLUE";     status = "BLUE brush"; break;
	case 54: cursorColor = "MAGENTA";  status = "MAGENTA brush"; break;
	case 55: cursorColor = "CYAN";     status = "CYAN brush"; break;
	case 56: cursorColor = "WHITE";    status = "WHITE brush"; break;

	  // Controls
	case 97: cursorX--;  moved = true; break;
	case 100: cursorX++; moved = true; break;
	case 119: cursorY--; moved = true; break;
	case 115: cursorY++; moved = true; break;

	case 106: pencilDown = true; status = "Pencil down"; moved = true; break; // pencil down
	case 107: pencilDown = false; status = "Pencil up"; break; // pencil up
	case 117: image.FillAllPixels( cursorColor ); break; // fill all
	case 104: image.SetPixelColor( cursorX, cursorY, cursorColor ); // poke color
	}

      if ( moved && pencilDown )
	{
	  image.SetPixelColor( cursorX, cursorY, cursorColor );
	}

      // cursor wrap
      if ( cursorX < 0 ) { cursorX = imageWidth-1; }
      else if ( cursorX >= imageWidth ) { cursorX = 0; }
      if ( cursorY < 0 ) { cursorY = imageHeight-1; }
      else if ( cursorY >= imageHeight ) { cursorY = 0; }
    }

  TerminalApp::Teardown();

  return 0;
}
