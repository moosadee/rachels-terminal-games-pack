#include "State_Game.h"
#include "../engine/TerminalApp.h"
#include <cstdlib>
#include <ctime>

State_Game::State_Game()
  : PUZWIDTH( 9 ), PUZHEIGHT( 9 )
{
  TerminalApp::Log( "Function begin", "State_Game::State_Game" );
  m_subState = "GAME";
  srand( time( NULL ) );
}

void State_Game::Setup()
{
  TerminalApp::Log( "Function begin", "State_Game::Setup" );
  State_Base::Setup();
  TerminalApp::SetDialogBorderColor( "BLACK-ON-WHITE" );
  TerminalApp::SetDialogBackgroundColor( "WHITE-ON-BLACK" );
  TerminalApp::SetDialogTextColor( "WHITE-ON-BLACK" );

  std::string gameType = TerminalApp::GetGlobalMessage( "game" );
  // rand-easy
  BuildPuzzle( gameType );
  m_cursorX = 4;
  m_cursorY = 4;
}

void State_Game::Teardown()
{
  TerminalApp::Log( "Function begin", "State_Game::Teardown" );
}

void State_Game::Update()
{
  TerminalApp::Log( "Function begin", "State_Game::Update()" );
  TerminalApp::Update();

  switch( TerminalApp::input)
  {
  case 260: // left arrow
  case 97: // 'a'
    m_cursorX--; break;
  case 261: // right arrow
  case 100: // 'd'
    m_cursorX++; break;
  case 259: // up arrow
  case 119: // 'w'
    m_cursorY--; break;
  case 258: // down arrow
  case 115: // 's'
    m_cursorY++; break;

    case 49: PlaceNumber( 1, m_cursorX, m_cursorY ); break;
    case 50: PlaceNumber( 2, m_cursorX, m_cursorY ); break;
    case 51: PlaceNumber( 3, m_cursorX, m_cursorY ); break;
    case 52: PlaceNumber( 4, m_cursorX, m_cursorY ); break;
    case 53: PlaceNumber( 5, m_cursorX, m_cursorY ); break;
    case 54: PlaceNumber( 6, m_cursorX, m_cursorY ); break;
    case 55: PlaceNumber( 7, m_cursorX, m_cursorY ); break;
    case 56: PlaceNumber( 8, m_cursorX, m_cursorY ); break;
    case 57: PlaceNumber( 9, m_cursorX, m_cursorY ); break;
  }

  if ( m_cursorX < 0 ) { m_cursorX = PUZWIDTH-1; }
  else if ( m_cursorX >= PUZWIDTH ) { m_cursorX = 0; }
  if ( m_cursorY < 0 ) { m_cursorY = PUZHEIGHT-1; }
  else if ( m_cursorY >= PUZHEIGHT ) { m_cursorY = 0; }



}

void State_Game::Draw()
{
  TerminalApp::DrawRectangle( 0, 0, m_screenWidth, m_screenHeight, "BLUE-ON-BLACK", ' ' );
  TerminalApp::DrawBorderedText( 2, 15, TerminalApp::ToString( m_cursorX ) + ", " + TerminalApp::ToString( m_cursorY ) );
  TerminalApp::DrawBorderedText( 20, 15, TerminalApp::ToString( TerminalApp::input ) );
  DrawPuzzle();

  TerminalApp::DrawBorderedTexts( 30, 2, { "[ARROWS]: Move cursor", "[1-9]: Place number" } );

  // Debug: Key code
  TerminalApp::DrawLine( 0, TerminalApp::screenHeight, TerminalApp::screenWidth, TerminalApp::screenHeight, "WHITE-ON-BLUE", ' ' );
  TerminalApp::DrawText( 0, TerminalApp::screenHeight, "WHITE-ON-BLUE", TerminalApp::ToString( TerminalApp::input ) );

  TerminalApp::RefreshScreen();
}

void State_Game::DrawPuzzle()
{
  TerminalApp::DrawBorderRectangle( 1,  1, 9,  5, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 9,  1, 17, 5, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 17, 1, 25, 5, "WHITE-ON-WHITE" );

  TerminalApp::DrawBorderRectangle( 1,  5, 9,  9, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 9,  5, 17, 9, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 17, 5, 25, 9, "WHITE-ON-WHITE" );

  TerminalApp::DrawBorderRectangle( 1,  9, 9,  13, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 9,  9, 17, 13, "WHITE-ON-WHITE" );
  TerminalApp::DrawBorderRectangle( 17, 9, 25, 13, "WHITE-ON-WHITE" );

  DrawSubPuzzle( 0, 0 );
  DrawSubPuzzle( 3, 0 );
  DrawSubPuzzle( 6, 0 );
  DrawSubPuzzle( 0, 3 );
  DrawSubPuzzle( 3, 3 );
  DrawSubPuzzle( 6, 3 );
  DrawSubPuzzle( 0, 6 );
  DrawSubPuzzle( 3, 6 );
  DrawSubPuzzle( 6, 6 );
}

void State_Game::DrawSubPuzzle( int startX, int startY )
{
  int firstX = 3;
  int firstY = 2;

  if ( startX == 3 ) { firstX = 5; }
  else if ( startX == 6 ) { firstX = 7; }
  if ( startY == 3 ) { firstY = 3; }
  else if ( startY == 6 ) { firstY = 4; }

  for ( int y = startY; y < startY+3; y++ )
  {
    for ( int x = startX; x < startX+3; x++ )
    {
      if ( m_cursorX == x && m_cursorY == y )
      {
        TerminalApp::DrawText( x*2+firstX, y+firstY, "YELLOW-ON-RED", TerminalApp::ToString( m_puzzle[x][y] ) );
      }
      else
      {
        TerminalApp::DrawText( x*2+firstX, y+firstY, "WHITE-ON-BLACK", TerminalApp::ToString( m_puzzle[x][y] ) );
      }
    }
  }
}

void State_Game::PlaceNumber( int number, int x, int y )
{
  m_puzzle[x][y] = number;
}

void State_Game::BuildPuzzle( std::string gameType )
{
  // Reset board
  for ( int y = 0; y < PUZHEIGHT; y++ )
  {
    for ( int x = 0; x < PUZWIDTH; x++ )
    {
      m_puzzle[x][y] = 0;
    }
  }

  // Generate board
  BuildPuzzleRecursive( 0, 0 );
}

bool State_Game::BuildPuzzleRecursive( int x, int y )
{
  TerminalApp::Log( "x: " + TerminalApp::ToString( x ) + ", y: " + TerminalApp::ToString( y ), "State_Game::BuildPuzzleRecursive" );

//  m_puzzle[x][y] = rand() % 9 + 1;
//  bool isOk = IsValidNumber( x, y );
//
//  if ( isOk )
//  {
//    int nextX = x+1;
//    int nextY = y;
//    if ( nextX >= 9 ) { y++; x=0; }
//    return BuildPuzzleRecursive( nextX, nextY );
//  }
//  else
//  {
//  }
  return false;
}

bool State_Game::IsValidNumber( int x, int y )
{
  return true;
}
