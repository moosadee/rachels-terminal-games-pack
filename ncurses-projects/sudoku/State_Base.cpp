#include "State_Base.h"
#include "../engine/TerminalApp.h"

State_Base::State_Base()
{
  TerminalApp::Log( "Function begin", "State_Base::State_Base" );
  m_nextState = "";
}

void State_Base::Setup()
{
  TerminalApp::Log( "Function begin", "State_Base::Setup()" );
  m_screenWidth = TerminalApp::screenWidth;
  m_screenHeight = TerminalApp::screenHeight;
}
