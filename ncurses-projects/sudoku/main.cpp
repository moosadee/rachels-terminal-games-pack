#include <iostream>
using namespace std;

#include "../engine/TerminalApp.h"
#include "../engine/PpmImage.h"
#include "State_Title.h"
#include "State_Game.h"

int main()
{
  TerminalApp::Setup();

  TerminalApp::Log( "Create state" );
  TerminalApp::SetGlobalMessage( "game", "rand-easy" ); // debug
//  State_Base* state = new State_Title;
  State_Base* state = new State_Game;
  state->Setup();

  TerminalApp::Log( "Start program loop" );
  bool done = false;
  while ( !done )
    {
      state->Draw();
      state->Update();

      if ( state->GetNextState() == "QUIT" )
	{
	  done = true;
	}
      if ( state->GetNextState() == "GAME" )
	{
	  TerminalApp::Log( "Switch state" );
	  state->Teardown();
	  delete state;
	  state = new State_Game;
	  state->Setup();
	}
    }

  TerminalApp::Log( "Teardown" );
  state->Teardown();
  delete state;

  TerminalApp::Teardown();

  cout << endl << " GOODBYE! " << endl;
  cout << "I guess you could visit moosadee.com if you want!" << endl << endl;
  return 0;
}
