#include "State_Title.h"
#include "../engine/TerminalApp.h"

State_Title::State_Title()
{
  TerminalApp::Log( "Function begin", "State_Title::State_Title" );
  m_subState = "TITLE";
}

void State_Title::Setup()
{
  TerminalApp::Log( "Function begin", "State_Title::Setup" );
  State_Base::Setup();
  TerminalApp::SetDialogBorderColor( "BLACK-ON-WHITE" );
  TerminalApp::SetDialogBackgroundColor( "WHITE-ON-RED" );
  TerminalApp::SetDialogTextColor( "WHITE-ON-RED" );
  m_title = "    MOOS-A-DEE'S TERMINAL SUDOKU    ";
  m_background.Open( "background.ppm" );
}

void State_Title::Teardown()
{
  TerminalApp::Log( "Function begin", "State_Title::Teardown" );
}

void State_Title::Update()
{
  TerminalApp::Log( "Function begin", "State_Title::Update()" );
  TerminalApp::Update();

  if ( TerminalApp::input == 48 ) // 0 - quit
    {
      if ( m_subState == "TITLE" )
	{
	  m_nextState = "QUIT";
	}
      else
	{
	  m_subState = "TITLE";
	}
    }
  else if ( TerminalApp::input == 49 ) // 1 - play
    {
      TerminalApp::SetGlobalMessage( "game", "rand-easy" );
      m_nextState = "GAME";
    }
  else if ( TerminalApp::input == 52 ) // 4 - help
    {
      m_subState = "HELP";
    }
}

void State_Title::Draw()
{
  TerminalApp::Log( "Function begin", "State_Title::Draw()" );
  m_background.Draw();

  if ( m_subState == "TITLE" )
    {
      TerminalApp::DrawBorderedTexts( 2, 1, { m_title } );

      TerminalApp::DrawBorderedTexts( 2, 8, {
      "LEVEL SELECT",
      "",
	  " [1] RANDOM PUZZLE (EASY) ",
	  " [4] HELP ",
	  "",
	  "[0] QUIT"
	} );
    }
  else if ( m_subState == "HELP" )
    {
      TerminalApp::DrawBorderedTexts( 4, 5, {
	  "Lorem ipsum dolor sit amet, consectetur adipiscing elit,",
	  "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
	  "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris",
	  "nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in",
	  "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla",
	  "pariatur. Excepteur sint occaecat cupidatat non proident, sunt in ",
	  "culpa qui officia deserunt mollit anim id est laborum.",
	  "",
	  "[0] BACK"
	} );
    }

  // Debug: Key code
  TerminalApp::DrawLine( 0, TerminalApp::screenHeight, TerminalApp::screenWidth, TerminalApp::screenHeight, "WHITE-ON-BLUE", ' ' );
  TerminalApp::DrawText( 0, TerminalApp::screenHeight, "WHITE-ON-BLUE", TerminalApp::ToString( TerminalApp::input ) );

  TerminalApp::RefreshScreen();
}
