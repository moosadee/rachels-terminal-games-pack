#ifndef _STATE_GAME
#define _STATE_GAME

#include <string>
#include <vector>
#include "State_Base.h"

class State_Game : public State_Base
{
 public:
  State_Game();
  virtual ~State_Game() { }
  virtual void Setup();
  virtual void Teardown();
  virtual void Update();
  virtual void Draw();

private:
  std::string m_title;
  std::string m_subState;
  int m_puzzle[9][9];
  int m_cursorX;
  int m_cursorY;
  const int PUZWIDTH;
  const int PUZHEIGHT;

  void DrawPuzzle();
  void PlaceNumber( int number, int x, int y );
  void DrawSubPuzzle( int startX, int startY );
  void BuildPuzzle( std::string gameType );
  bool BuildPuzzleRecursive( int x, int y );
  bool IsValidNumber( int x, int y );
};

#endif
