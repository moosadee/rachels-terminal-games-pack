#ifndef _TERMINALAPP
#define _TERMINALAPP

#include <map>
#include <string>
#include <sstream>
#include <vector>

class TerminalApp
{
public:
  static void Setup();
  static void Teardown();
  static void RefreshScreen();
  static void Update();
  static void DrawChar( int x, int y, std::string colorKey, char symbol );
  static void DrawText( int x, int y, std::string colorKey, std::string text );
  static void DrawLine( int x1, int y1, int x2, int y2, std::string colorKey, char symbol );
  static void DrawRectangle( int left, int top, int right, int bottom, std::string colorKey, char symbol );
  // More UI-like elements
  static void SetDialogBorderColor( std::string colorKey );
  static void SetDialogBackgroundColor( std::string colorKey );
  static void SetDialogTextColor( std::string colorKey );
  static void DrawBorderRectangle( int left, int top, int right, int bottom, std::string colorKey = dialogBorderColor );
  static void DrawBorderedText( int x, int y, std::string text, std::string borderKey = dialogBorderColor, std::string backgroundKey = dialogBackgroundColor, std::string textKey = dialogTextColor );
  static void DrawBorderedTexts( int x, int y, std::vector<std::string> text, std::string borderKey = dialogBorderColor, std::string backgroundKey = dialogBackgroundColor, std::string textKey = dialogTextColor );

  static void SetGlobalMessage( std::string key, std::string value );
  static std::string GetGlobalMessage( std::string key );

  // User input
  static bool IsKeyPressed( int type );

  // Logger
  static double GetTimestamp();
  static std::string GetFormattedTimestamp();
  static void Log( std::string message );
  static void Log( std::string message, std::string location );
  static void LogError( std::string message, std::string location );

  static int screenWidth;
  static int screenHeight;
  static int input;
  static std::map<std::string, int> colorPairs;
  static std::ofstream log;
  static std::string dialogBorderColor;
  static std::string dialogBackgroundColor;
  static std::string dialogTextColor;
  static std::map<std::string, std::string> globalMessage;


  // Templated stuff
  template <typename T>
  static std::string ToString( const T& value )
  {
    std::stringstream ss;
    ss << value;
    return ss.str();
  }
};

#endif
