#include "TerminalApp.h"

#include <ncurses.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>

int TerminalApp::screenWidth = 80;
int TerminalApp::screenHeight = 24;
int TerminalApp::input = 0;
std::string TerminalApp::dialogBorderColor = "";
std::string TerminalApp::dialogBackgroundColor = "";
std::string TerminalApp::dialogTextColor = "";
std::map<std::string, int> TerminalApp::colorPairs;
std::ofstream TerminalApp::log;
std::map<std::string, std::string> TerminalApp::globalMessage;

void TerminalApp::Setup()
{
  log.open( "log.txt" );
  log << std::left;
  TerminalApp::Log( "Function begin", "TerminalApp::Setup" );
  initscr(); // Init ncurses
  keypad( stdscr, TRUE );
  noecho(); // No echo for getch
  //	raw();
  start_color();

  std::vector<int> colors             = { COLOR_BLACK, COLOR_RED, COLOR_GREEN, COLOR_YELLOW, COLOR_BLUE, COLOR_MAGENTA, COLOR_CYAN, COLOR_WHITE };
  std::vector<std::string> colorNames = { "BLACK", "RED", "GREEN", "YELLOW", "BLUE", "MAGENTA", "CYAN", "WHITE" };
  std::string debugText = "Colors: ";
  int pairCount = 0;
  for ( size_t bg = 0; bg < colors.size(); bg++ )
    {
      for ( size_t fg = 0; fg < colors.size(); fg++ )
	{
	  std::string name = colorNames[fg] + "-ON-" + colorNames[bg];
	  debugText += name + "=" + ToString( pairCount ) + "...";
	  colorPairs[name] = pairCount;
	  init_pair( pairCount, colors[fg], colors[bg] ); // int init_pair(short pair, short f, short b);
	  pairCount++;
	}
    }
  Log( debugText );
}

void TerminalApp::Teardown()
{
  TerminalApp::Log( "Function begin", "TerminalApp::Teardown" );
  endwin(); // end curses mode
  log.close();
}

void TerminalApp::Update()
{
  input = getch();
}

void TerminalApp::DrawChar( int x, int y, std::string colorKey, char symbol )
{
  attron( COLOR_PAIR( colorPairs[colorKey] ) );
  mvaddch( y, x, symbol );
}

void TerminalApp::DrawText( int x, int y, std::string colorKey, std::string text )
{
  attron( COLOR_PAIR( colorPairs[colorKey] ) );
  mvprintw( y, x, text.c_str(), 0 );
}

void TerminalApp::DrawLine( int x1, int y1, int x2, int y2, std::string colorKey, char symbol )
{
  int x = x1;
  int y = y1;
  bool done = false;
  while ( !done )
    {
      DrawChar( x, y, colorKey, symbol );
      if ( x == x2 && y == y2 )
	{
	  done = true;
	}

      if      ( x < x2 ) { x++; }
      else if ( x > x2 ) { x--; }
      if      ( y < y2 ) { y++; }
      else if ( y > y2 ) { y--; }
    }
}

void TerminalApp::DrawRectangle( int left, int top, int right, int bottom, std::string colorKey, char symbol )
{
  for ( int y = top; y <= bottom; y++ )
    {
      for ( int x = left; x <= right; x++ )
	{
	  DrawChar( x, y, colorKey, symbol );
	}
    }
}

void TerminalApp::SetDialogBorderColor( std::string colorKey )
{
  dialogBorderColor = colorKey;
}

void TerminalApp::SetDialogBackgroundColor( std::string colorKey )
{
  dialogBackgroundColor = colorKey;
}

void TerminalApp::SetDialogTextColor( std::string colorKey )
{
  dialogTextColor = colorKey;
}

void TerminalApp::DrawBorderRectangle( int left, int top, int right, int bottom, std::string colorKey /*= dialogBorderColor */ )
{
  DrawLine( left, top, right, top, colorKey, ' ' );
  DrawLine( left, bottom, right, bottom, colorKey, ' ' );
  DrawLine( left, top, left, bottom, colorKey, ' ' );
  DrawLine( right, top, right, bottom, colorKey, ' ' );
}

void TerminalApp::DrawBorderedText( int x, int y, std::string text, std::string borderKey /*= dialogBorderColor*/, std::string backgroundKey /*= dialogBackgroundColor*/, std::string textKey /*= dialogTextColor*/ )
{
  // Draw bordered region first
  int left = x;
  int right = x + text.size() + 3;
  int top = y;
  int bottom = top + 4;
  DrawBorderRectangle( left, top, right, bottom, borderKey );
  DrawRectangle( left+1, top+1, right-1, bottom-1, backgroundKey, ' ' );
  // Draw text
  DrawText( x+2, y+2, textKey, text );
}

void TerminalApp::DrawBorderedTexts( int x, int y, std::vector<std::string> text, std::string borderKey /*= dialogBorderColor*/, std::string backgroundKey /*= dialogBackgroundColor*/, std::string textKey /*= dialogTextColor*/ )
{
  int longestText = 0;
  for ( auto& tx : text )
    {
      if ( tx.size() > longestText ) { longestText = tx.size(); }
    }

  // Draw bordered region first
  int left = x;
  int right = x + longestText + 3;
  int top = y;
  int bottom = top + text.size() + 3;
  DrawBorderRectangle( left, top, right, bottom, borderKey );
  DrawRectangle( left+1, top+1, right-1, bottom-1, backgroundKey, ' ' );
  // Draw text
  for ( size_t i = 0; i < text.size(); i++ )
    {
      DrawText( x+2, y+2+i, textKey, text[i] );
    }
}


bool TerminalApp::IsKeyPressed( int type )
{
  return ( input == type );
}

void TerminalApp::RefreshScreen()
{
  refresh();
}

std::string TerminalApp::GetFormattedTimestamp()
{

    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        time_t timestamp = GetTimestamp();
        struct tm timeinfo;
        char buffer[80];
        time( &timestamp );
        localtime_s ( &timeinfo, &timestamp );
        // http://www.cplusplus.com/reference/ctime/strftime/
        strftime( buffer, 80, "%H:%M:%S", &timeinfo );

        std::string str( buffer );
        return str;
    #else
            time_t timestamp = GetTimestamp();
        struct tm* timeinfo;
        char buffer[80];
        time( &timestamp );
        timeinfo = localtime ( &timestamp );
        // http://www.cplusplus.com/reference/ctime/strftime/
        strftime( buffer, 80, "%H:%M:%S", timeinfo );

        std::string str( buffer );
        return str;
    #endif

	return "x";
}

double TerminalApp::GetTimestamp()
{
  std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
  return std::chrono::system_clock::to_time_t( now );
}

void TerminalApp::Log( std::string message )
{
  Log( message, "" );
}

void TerminalApp::Log( std::string message, std::string location )
{
  log
    << std::setw( 10 ) << GetFormattedTimestamp()
    << std::setw( 30 ) << location
    << "\t" <<  message << std::endl;
}

void TerminalApp::LogError( std::string message, std::string location )
{
  log << "** "
    << std::setw( 10 ) << GetFormattedTimestamp()
    << std::setw( 30 ) << location
    << "\t" <<  message << " **" << std::endl;
}

void TerminalApp::SetGlobalMessage( std::string key, std::string value )
{
  globalMessage[key] = value;
}

std::string TerminalApp::GetGlobalMessage( std::string key )
{
  if ( globalMessage.find( key ) != globalMessage.end() )
  {
    return globalMessage[key];
  }
  return "NOTFOUND";
}
