#include "PpmImage.h"
#include "TerminalApp.h"

#include <fstream>
#include <iostream>

PpmImage::PpmImage()
{
}

PpmImage::PpmImage( std::string filename, int width, int height )
{
  Create( filename, width, height );
}

PpmImage::PpmImage( std::string filename )
{
  Open( filename );
}

void PpmImage::Create( std::string filename, int width, int height )
{
  m_filename = filename;
  CreateCanvas( width, height );
}

void PpmImage::Open( std::string filename )
{
  TerminalApp::Log( "Open " + filename, "PpmImage::Open" );

  if ( filename == "" ) { filename = m_filename; }
  std::ifstream input( filename );
  if ( input.fail() )
    {
      TerminalApp::LogError( "Error opening file \"" + filename + "\"", "PpmImage::Open" );
      return;
    }
  std::string buffer;
  getline( input, buffer ); // P3
  getline( input, buffer ); // Comment
  input >> m_width >> m_height;
  input >> buffer; // Color depth

  TerminalApp::Log( "Read in pixels", "PpmImage::Open" );
  Pixel newPixel;
  int x = 0, y = 0;
  while ( input >> newPixel.r >> newPixel.g >> newPixel.b )
    {
      // Figure out ncurses color
      newPixel.ncursesColor = RgbToNcursesColor( newPixel.r, newPixel.g, newPixel.b );

      // Set up coordinates
      newPixel.x = x;
      newPixel.y = y;
      x++;
      if ( x >= m_width )
	{
	  x = 0;
	  y++;
	}

      m_pixels.push_back( newPixel );
    }
}

void PpmImage::Save( std::string filename )
{
  if ( filename == "" ) { filename = m_filename; }
  std::ofstream output( filename );

  output << "P3" << std::endl;
  output << "# Created with Cursed Paint" << std::endl;
  output << m_width << " " << m_height << std::endl;
  output << "255" << std::endl;
  for ( auto& px : m_pixels )
    {
      output << px.r << std::endl << px.g << std::endl << px.b << std::endl;
    }
}

int PpmImage::GetWidth() const
{
  return m_width;
}

int PpmImage::GetHeight() const
{
  return m_height;
}

void PpmImage::SetDimensions( int width, int height )
{
  m_width = width;
  m_height = height;
}

std::vector<Pixel>& PpmImage::GetPixels()
{
  return m_pixels;
}

void PpmImage::CreateCanvas( int width, int height )
{
  SetDimensions( width, height );
  for ( int y = 0; y < height; y++ )
    {
      for ( int x = 0; x < width; x++ )
        {
          Pixel newPixel;
	  newPixel.SetXy( x, y );
	  if ( ( x + y ) % 2 == 0 )
	    {
	      newPixel.SetRgb( 255, 0, 255 );
	      newPixel.ncursesColor = "MAGENTA";
	    }
	  else
	    {
	      newPixel.SetRgb( 0, 255, 255 );
	      newPixel.ncursesColor = "CYAN";
	    }
	  m_pixels.push_back( newPixel );
        }
    }
}

std::string PpmImage::RgbToNcursesColor( int r, int g, int b )
{
  if      ( r == 0 && g == 0 && b == 0 ) { return "BLACK"; }
  else if ( r == 0 && g == 0 && b == 255 ) { return "BLUE"; }
  else if ( r == 0 && g == 255 && b == 0 ) { return "GREEN"; }
  else if ( r == 0 && g == 255 && b == 255 ) { return "CYAN"; }
  else if ( r == 255 && g == 0 && b == 0 ) { return "RED"; }
  else if ( r == 255 && g == 0 && b == 255 ) { return "MAGENTA"; }
  else if ( r == 255 && g == 255 && b == 0 ) { return "YELLOW"; }
  else if ( r == 255 && g == 255 && b == 255 ) { return "WHITE"; }
  else { return "MAGENTA"; } // default
}

RGB PpmImage::NcursesColorToRgb( std::string color )
{
  if ( color == "BLACK" ) { return RGB( 0, 0, 0 ); }
  else if ( color == "BLUE" ) { return RGB( 0, 0, 255 ); }
  else if ( color == "GREEN" ) { return RGB( 0, 255, 0 ); }
  else if ( color == "CYAN" ) { return RGB( 0, 255, 255 ); }
  else if ( color == "RED" ) { return RGB( 255, 0, 0 ); }
  else if ( color == "MAGENTA" ) { return RGB( 255, 0, 255 ); }
  else if ( color == "YELLOW" ) { return RGB( 255, 255, 0 ); }
  else if ( color == "WHITE" ) { return RGB( 255, 255, 255 ); }
  else { return RGB( 255, 0, 255 ); }
}

std::string PpmImage::GetGoodTextColor( std::string backgroundColor )
{
  if ( backgroundColor == "BLACK" || backgroundColor == "BLUE" || backgroundColor == "RED" )
    {
      return "WHITE";
    }
  else
    {
      return "BLACK";
    }
}

void PpmImage::SetPixelColor( int x, int y, std::string ncursesColor )
{
  for ( auto& px : m_pixels )
    {
      if ( px.x == x && px.y == y )
	{
	  px.ncursesColor = ncursesColor;
	  RGB rgb = NcursesColorToRgb( ncursesColor );
	  px.r = rgb.r;
	  px.g = rgb.g;
	  px.b = rgb.b;
	  return;
	}
    }
}

void PpmImage::FillAllPixels( std::string ncursesColor )
{
  RGB rgb = NcursesColorToRgb( ncursesColor );
  for ( auto& px : m_pixels )
    {
      px.ncursesColor = ncursesColor;
      px.r = rgb.r;
      px.g = rgb.g;
      px.b = rgb.b;
    }
}

std::string PpmImage::GetFilename() const
{
  return m_filename;
}

void PpmImage::Draw() const
{
  for ( const auto& pixel : m_pixels )
    {
      TerminalApp::DrawChar( pixel.x, pixel.y, "WHITE-ON-" + pixel.ncursesColor, ' ' );
    }
}
