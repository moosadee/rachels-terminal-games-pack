#include "Game.hpp"
#include "../engine/ScreenDrawer.h"

#include <cstdlib>
#include <ctime>

int main()
{
    // Initialize random numbers
    srand( time( NULL ) );

    ScreenDrawer::Setup( 80, 20 );
    
    // Start game
    Game game;
    game.Run();

    ScreenDrawer::Teardown();
    
    return 0;
}
